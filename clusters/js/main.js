﻿window.addEventListener('DOMContentLoaded', function () {
	function getRandomColor() {
		var r = Math.random();
		var g = Math.random();
		var b = Math.random();
		return new THREE.Color(r, g, b);
	}

	(function main() {

		let numColors = 4;
		let colors = [];
		for (var i = 0; i < numColors; i++) {
			let ii = i + 1;
			let r = ii % 2;
			let g = Math.floor(ii / 2) % 2;
			let b = Math.floor(ii / 4) % 4;
			colors[i] = new THREE.Color(r, g, b);

			//colors[i] = new THREE.Color(getRandomColor());
		}

		let numRows = 4;
		let numColumns = 5;
		let dimensions = { numRows, numColumns };

		let clusters = clusterController();
		let grid = clusters.initialiseGrid(dimensions, numColors);
		numColumns = dimensions.numColumns;
		numRows = dimensions.numRows;

		let blocks = blockController();
		blocks.setupThreeJS();


		let gridBlocks = blocks.setupGridBlocks(grid, colors, numColumns, numRows);



		document.addEventListener("keyup", tag);

		function tag(e) {
			if (e.key !== " ") return;

			document.removeEventListener("keyup", tag);
			document.addEventListener("keyup", hide);

			let clone = grid.map(function (arr) {
				return arr.slice();
			});

			let result = clusters.findCluster(clone);

			clusters.tagCluster(grid, result.clusterCol, result.clusterRow);

			for (let row = 0; row < numRows; row++) {
				for (let col = 0; col < numColumns; col++) {
					if (grid[col][row] == 0) {
						blocks.tagBlock(col, row);
					}
				}
			}
		}

		function hide() {
			document.removeEventListener("keyup", hide);
			document.addEventListener("keyup", dropByOne);

			for (let row = 0; row < numRows; row++) {
				for (let col = 0; col < numColumns; col++) {
					if (blocks.isTagged(col, row)) {
						blocks.hideBlock(col, row);
					}
				}
			}
		}

		function dropByOne() {
			isDropping = false;
			document.removeEventListener("keyup", dropByOne);

			// if the cell below is empty, swap
			for (let row = 1; row < numRows; row++) {
				for (let col = 0; col < numColumns; col++) {
					if (grid[col][row - 1] == 0 && grid[col][row] != 0) {
						gridBlocks[col][row - 1].position.y = gridBlocks[col][row].position.y;

						droppingBlocks.push(gridBlocks[col][row]);

						let t = gridBlocks[col][row];
						gridBlocks[col][row] = gridBlocks[col][row - 1];
						gridBlocks[col][row - 1] = t;

						grid[col][row - 1] = grid[col][row];
						grid[col][row] = 0;

						isDropping = true;
						animateBlocks = animateDropBlocks;
					}
				}
			}

			if (!isDropping) {
				animateBlocks = nop;
				document.addEventListener("keyup", prepForDropNewBlocks);
				//aaa();
			}
		}

		function nop() { };
		let animateBlocks = nop;

		let droppingBlocks = [];
		let isDropping = false;
		let dropCount = 5;
		let dropStep = dropCount;
		function animateDropBlocks() {
			if (dropCount <= 0) {
				dropCount = dropStep;
				droppingBlocks = [];
				dropByOne();
				isDropping = false;

				if (droppingBlocks.length > 0)
					isDropping = true;
				else
					document.addEventListener("keyup", prepForDropNewBlocks);

				return;
			}
			dropCount--;

			for (let i = 0; i < droppingBlocks.length; i++) {
				blocks.moveMeshBy(droppingBlocks[i], 0, -1.25 / dropStep, 0);
			}
		}


		let offset;
		function prepForDropNewBlocks() {
			document.removeEventListener("keyup", prepForDropNewBlocks);
			document.addEventListener("keyup", addNewBlocksToTop);

			for (let row = 0; row < numRows; row++) {
				for (let col = 0; col < numColumns; col++) {
					if (grid[col][row] == 0) {
						droppingBlocks.push(gridBlocks[col][row]);
					}
				}
			}

			if (droppingBlocks.length > 0) {
				let lowest = droppingBlocks[0].position.y;
				offset = Math.floor(0.5 * (numRows) + 2) * 1.25 - lowest;
				for (let i = 0; i < droppingBlocks.length; i++) {
					droppingBlocks[i].position.y += offset;
					droppingBlocks[i].visible = true;
				}
			}
		}

		function animateDropNewBlocks() {
			let dropDistance = offset / 5;
			if (dropCount <= 0) {
				dropCount = dropStep;

				animateBlocks = nop;
				droppingBlocks = [];
				document.addEventListener("keyup", tag);

				return;
			}
			dropCount--;

			for (let i = 0; i < droppingBlocks.length; i++) {
				blocks.moveMeshBy(droppingBlocks[i], 0, -dropDistance, 0);
			}
		}


		// add new blocks to top
		function addNewBlocksToTop() {
			document.removeEventListener("keyup", addNewBlocksToTop);
			clusters.newBlocks(grid, numColors);
			blocks.showNewBlocks(grid, colors);
			isDropping = true;
			animateBlocks = animateDropNewBlocks;
		}



		// Calculate milliseconds in a year
		const minute = 1000 * 60;
		const hour = minute * 60;
		const day = hour * 24;

		// Divide Time with a year
		const d = new Date();
		let days = Math.round(d.getTime() / day);
		days %= 3;


		let capturer = new CCapture({
			//format: 'webm',
			format: 'gif', workersPath: '../../common/js/',
			//verbose: true,
			framerate: 60,
			quality: 90,
			//	startTime: 1,
			timeLimit: 10,
		});


		let canvas = document.getElementById("webgl-canvas");

		let animate = function () {
			requestAnimationFrame(animate);

			animateBlocks();

			blocks.update();

			capturer.capture(canvas);
		};

		document.addEventListener("keyup", stop);
		function stop(e) {
			if (e.key === "Enter") {
				capturer.start();
			}
			if (e.key === "Escape") {
				capturer.stop();

				// default save, will download automatically a file called {name}.extension (webm/gif/tar)
				capturer.save();
			}
		}

		animate();
	})();

});
