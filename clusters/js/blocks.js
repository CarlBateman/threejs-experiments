﻿function blockController() {

	let scene = new THREE.Scene();
	let materialTag = new THREE.MeshPhongMaterial({ color: new THREE.Color(1, 1, 1) });
	let orbitControls;
	let canvas;
	let renderer;
	let camera;
	let gridBlocks = [];
	let pivot;

	return { setupThreeJS, setupGridBlocks, update, tagBlock, hideBlock, isTagged, moveMeshBy, showNewBlocks }

	function isTagged(col, row) {
		return gridBlocks[col][row].material == materialTag
	}

	function moveMeshBy(mesh, x, y, z) {
		mesh.position.x += x;
		mesh.position.y += y;
		mesh.position.z += z;
	}

	function tagBlock(col, row) {
		gridBlocks[col][row].material = materialTag;
	}

	function hideBlock(col, row) {
		gridBlocks[col][row].visible = false;
	}

	function setCameraAndRenderer() {
		canvas.style.width = '';
		canvas.style.height = '';
		let w = canvas.clientWidth;
		let h = canvas.clientHeight;

		renderer.setSize(w, h);
		renderer.setViewport(0, 0, w, h);
		//renderer.setSize(window.innerWidth, window.innerHeight);

		camera.aspect = w / h;
		camera.updateProjectionMatrix();
	}

	function setupThreeJS() {

		window.addEventListener('resize', setCameraAndRenderer);

		renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
		renderer.domElement.id = "webgl-canvas";
		document.body.appendChild(renderer.domElement);

		canvas = renderer.domElement;
		canvas.classList.add("centered");


		camera = new THREE.PerspectiveCamera(75, canvas.clientWidth / canvas.clientHeight, 1, 10000);
		camera.position.z = 10;
		//camera.zoom = 20;

		setCameraAndRenderer();

		let spotLight = new THREE.DirectionalLight(0xffffff, 0.5);
		spotLight.position.x = 20;
		spotLight.position.y = 20;
		scene.add(spotLight);


		let light = new THREE.AmbientLight(0xffffff, 0.5);
		scene.add(light);

		const loader = new THREE.TextureLoader();
		const texture = loader.load('../../common/img/square-edge.gif');
		texture.wrapS = THREE.RepeatWrapping;
		texture.wrapT = THREE.RepeatWrapping;
		texture.repeat.x = 50;
		texture.repeat.y = 50;

		const planeGeometry = new THREE.PlaneGeometry(150, 150);
		const planeMaterial = new THREE.MeshBasicMaterial({
			color: "lightblue",
			map: texture
		});
		const planeMesh = new THREE.Mesh(planeGeometry, planeMaterial);
		planeMesh.position.z = -10;
		planeMesh.rotation.z = -1;
		planeMesh.rotation.y = -1;
		scene.add(planeMesh);


		pivot = new THREE.Object3D();
		scene.add(pivot);
		//const geometry = new THREE.SphereGeometry(.5, 32, 16);
		//const material = new THREE.MeshBasicMaterial({ color: 0xffff00 });
		//const sphere = new THREE.Mesh(geometry, material);
		//scene.add(sphere);
		//const clone = grid.slice();
		//const clone = [...grid];
		//const clone = [].concat(grid);
		//let clone = grid.map(function (arr) {
		//	return arr.slice();
		//});


		orbitControls = new THREE.OrbitControls(camera, renderer.domElement);

		const pmremGenerator = new THREE.PMREMGenerator(renderer);
		pmremGenerator.compileEquirectangularShader();
		let exrCubeRenderTarget, exrBackground;
		// environment
		new THREE.EXRLoader().load('../../img/mutianyu_4k.exr', function (texture) {

				exrCubeRenderTarget = pmremGenerator.fromEquirectangular(texture);
				exrBackground = exrCubeRenderTarget.texture;

				texture.dispose();


			//scene.background = exrBackground;
			});


	}

	function showNewBlocks(grid, colors) {
		let numColumns = grid.length, numRows = grid[0].length;
		for (let col = 0; col < numColumns; col++) {
			for (let row = 0; row < numRows; row++) {
				if (gridBlocks[col][row].material === materialTag) {
					let val = grid[col][row];

					let color = colors[val - 1];
					//let material = new THREE.MeshPhongMaterial({ color: color });
					const material = new THREE.MeshPhysicalMaterial({
						roughness: 0,
						transmission: 1,
						thickness: 0.5, // Add refraction!
						color: color,
					});
					gridBlocks[col][row].material = material;
					gridBlocks[col][row].visible = true;
				}
			}
		}
	}

	function setupGridBlocks(grid, colors, numColumns, numRows) {
		gridBlocks = [];
		for (let col = 0; col < numColumns; col++) {
			let columnBlocks = [];

			for (let row = 0; row < numRows; row++) {
				let val = grid[col][row];

				let color = colors[val - 1];
				//let material = new THREE.MeshPhongMaterial({ color: color });
				const material = new THREE.MeshPhysicalMaterial({
					roughness: 0,
					transmission: 1,
					thickness: 0.5, // Add refraction!
					color: color,
				});

				//let geometry = new THREE.BoxGeometry(1, 1, 1);
				let geometry = createBoxWithRoundedEdges(1, 1, 1, .25, 8);

				let mesh = new THREE.Mesh(geometry, material);
				mesh.position.x = 1.25 * (col - (numColumns-1) / 2);
				mesh.position.y = 1.25 * (row - (numRows - 1) / 2);

				columnBlocks[row] = mesh;
				pivot.add(mesh);
			}
			gridBlocks[col] = columnBlocks;
		}

		function createBoxWithRoundedEdges(width, height, depth, radius0, smoothness) {
			let shape = new THREE.Shape();
			let eps = 0.00001;
			let radius = radius0 - eps;
			shape.absarc(eps, eps, eps, -Math.PI / 2, -Math.PI, true);
			shape.absarc(eps, height - radius * 2, eps, Math.PI, Math.PI / 2, true);
			shape.absarc(width - radius * 2, height - radius * 2, eps, Math.PI / 2, 0, true);
			shape.absarc(width - radius * 2, eps, eps, 0, -Math.PI / 2, true);
			let geometry = new THREE.ExtrudeBufferGeometry(shape, {
				depth: depth - radius0 * 2,
				bevelEnabled: true,
				bevelSegments: smoothness * 2,
				steps: 1,
				bevelSize: radius,
				bevelThickness: radius0,
				curveSegments: smoothness
			});

			geometry.center();

			return geometry;
		}

		return gridBlocks;
	}

	function update() {
		orbitControls.update();

		renderer.render(scene, camera);
	}
}