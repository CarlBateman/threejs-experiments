﻿// TODO isolated cluster values
// TODO multiple clusters with same value or same count

function clusterController() {
	return { findCluster, tagCluster, initialiseGrid, newBlocks };

	function newBlocks(grid, numColors) {
		let numColumns = grid.length, numRows = grid[0].length;
		for (let row = 0; row < numRows; row++) {
			for (let col = 0; col < numColumns; col++) {
				if (grid[col][row] == 0) {
					let val = Math.ceil(Math.random() * numColors)
					grid[col][row] = val;
				}
			}
		}
	}


	function initialiseGrid(dimensions, numColors) {
		let debug = false;
		let grid = [];

		if (debug) {
			//grid = [[3,1,1,1,2],   [2,3,5,6,3],    [3,5,4,6,5] ,    [2,5,4,7,2] ,    [5,4,6,1,3]];
			//grid = [[2, 1, 3, 1, 4],   [3, 2, 3, 1, 4,],    [4,3,1,3,4] ,    [4,3,1,4,1]];
			//grid = [[4, 4, 4, 1], [1, 4, 4, 3], [3, 3, 1, 1], [1, 2, 3, 3], [2, 3, 4, 4]];
			//grid = [[4, 2], [1, 2], [1, 2]];
			//grid = [[2, 2, 2], [4, 1, 1]];. Fuck off
			//grid = [[3, 3, 3, 1, 4], [3, 2, 3, 1, 4,], [4, 3, 3, 3, 4], [4, 3, 1, 4, 1]];
			grid = [[1,1,4], [2,3,4]];
			numColumns = grid.length;
			numRows = grid[0].length
			dimensions.numColumns = numColumns;
			dimensions.numRows = numRows;
		} else {
			numColumns = dimensions.numColumns;
			numRows = dimensions.numRows;
		}


		for (let col = 0; col < numColumns; col++) {
			let column = [];
			if (debug)
				column = grid[col];

			for (let row = 0; row < numRows; row++) {
				let val = Math.ceil(Math.random() * numColors)
				if (debug) {
					val = grid[col][row];
				} else {
					column[row] = val;
				}

			}
			grid[col] = column;
		}

		return grid;
	}

	function findCluster(grid) {
		let currentVal, count, clusterRow, clusterCol;
		let numColumns = grid.length, numRows = grid[0].length;
		let highest = 0;

		let val = 0;
		for (let row = 0; row < numRows; row++) {
			for (let col = 0; col < numColumns; col++) {

				if (grid[col][row] < 0) {
					continue;
				}
				currentVal = grid[col][row];
				count = 0;

				count = findNeighbour(grid, col, row);

				if (count > highest) {
					val = currentVal;
					highest = count;
					clusterRow = row;
					clusterCol = col;
				}
			}
		}

		return { highest, val, clusterRow, clusterCol };
	}

	function findNeighbour(grid, col, row) {
		let numColumns = grid.length, numRows = grid[0].length;
		let count = 0;
		let val = grid[col][row];

		find(col, row);

		function find(col, row) {
			count++;
			if (grid[col][row] < 0) return;
			grid[col][row] *= -1;

			// look up
			if (row + 1 <= numRows - 1 && grid[col][row + 1] == val)
				find(col, row + 1);

			// look right
			if (col + 1 <= numColumns - 1 && grid[col + 1][row] == val)
				find(col + 1, row);

			// look left
			if (col - 1 >= 0 && grid[col - 1][row] == val)
				find(col - 1, row);

			// look down
			if (row - 1 >= 0 && grid[col][row - 1] == val)
				find(col, row - 1);

		}
		return count;
	}

	function tagCluster(grid, col, row) {
		let numColumns = grid.length, numRows = grid[0].length;
		let val = grid[col][row];

		tag(col, row);

		function tag(col, row) {
			if (grid[col][row] == 0) return;
			grid[col][row] = 0;

			// look down
			if (row + 1 <= numRows - 1 && grid[col][row + 1] == val)
				tag(col, row + 1);

			// look right
			if (col + 1 <= numColumns - 1 && grid[col + 1][row] == val)
				tag(col + 1, row);

			// look left
			if (col - 1 >= 0 && grid[col - 1][row] == val)
				tag(col - 1, row);

			// look up
			if (row - 1 >= 0 && grid[col][row - 1] == val)
				tag(col, row - 1);

		}
	}

	function findFinalPosition(grid) {
		for (let i = 0; i < grid.length; i++) {
			let column = grid[i];

			tagFinalPosition(column);
		}

		function tagFinalPosition(column) {
			for (let i = 0; i < column.length; i++) {
				column[i] = { val: column[i], index: i };
			}
		}

		collapseTaggedColumns(grid);

	}

	function collapseTaggedColumns(grid, start, end) {
		let i = start;
		//while (i < grid.length && collapseColumn(grid, i) ) {
		//	i++;
		//}

		for (i = 0; i < grid.length; i++) {
			collapseTaggedColumn(grid, i)
		}
	}

	function collapseTaggedColumn(grid, col) {
		let temp = grid[col].filter(function (val) {
			return val.val != 0;
		});

		let column = grid[col];

		if (column.length == temp.length) {
			return false;
		}

		temp = temp.concat(Array(column.length - temp.length).fill({ val: 0, index: 0 }));//.concat(temp);

		//console.log(temp);
		grid[col] = temp;

		return true;
	}

	function collapseColumns(grid, start, end) {
		let i = start;
		//while (i < grid.length && collapseColumn(grid, i) ) {
		//	i++;
		//}

		for (i = 0; i < grid.length; i++) {
			collapseColumn(grid, i)
		}
	}

	function collapseColumn(grid, col) {
		let temp = grid[col].filter(function (val) {
			return val != 0;
		});

		let column = grid[col];

		if (column.length == temp.length) {
			return false;
		}

		temp = temp.concat(Array(column.length - temp.length).fill(0));//.concat(temp);

		//console.log(temp);
		grid[col] = temp;

		return true;
	}


	function dumpArray(grid) {
		let numColumns = grid.length, numRows = grid[0].length;

		for (let row = numRows - 1; row >= 0; row--) {
			let line = "row: " + row + " | ";
			for (let col = 0; col < numColumns; col++) {
				//console.log(grid[row][col] + ", ");
				let str = ("  " + grid[col][row]).slice(-2);
				line += str + ", ";
				//line += grid[row][col] + ", ";
			}
			console.log(line);
		}
		console.log("");
	}
}