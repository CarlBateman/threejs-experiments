﻿function addChess(scene, topright) {
  let controller = makeController();

  let canvasMin = -topright.y;
  let canvasMax = topright.y;

  function update() {
    let speed = setSpeed();
    let rSpeed = setRotationSpeed();

    return function (mesh, topright) {
      let canvasMin = -topright.y;
      let canvasMax = topright.y;

      mesh.rotation.x += rSpeed.x;
      mesh.rotation.y += rSpeed.y;

      mesh.position.y += speed;

      if (mesh.position.x < -topright.x) {
        setPositionX(mesh, topright);
      }
      if (mesh.position.x > -(topright.x / 2)) {
        setPositionX(mesh, topright);
      }

      if (mesh.position.y > canvasMax * 1.1) {
        mesh.position.y = canvasMin * 1.2;
        speed = setSpeed();
        rspeed = setRotationSpeed();
        setPositionX(mesh, topright);

        let color = getRandomColor();
        mesh.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material.color = color;
            child.castShadow = true;
            child.receiveShadow = true;
          }
        });
      }
    }
  }

  let href = window.location.href;
  let modelpath = "/models/chess";
  if (!href.includes("localhost"))
    modelpath = "/threejs-experiments" + modelpath;

  let manager = new THREE.LoadingManager();
  manager.onProgress = function (item, loaded, total) {
      console.log(item, loaded, total);
  };


  function loadOBJ(model) {
    function handleModelLoaded(model) {
      function initiliaseChessPiece(object) {
        let color = getRandomColor();
        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material.color = color;
          }
        });

        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.castShadow = true;
            child.receiveShadow = true;
          }
        });

        object.name = model.name;
        object.scale.set(0.6, 0.6, 0.6);

        setPositionX(object, topright);
        object.position.y = getRandomArbitrary(canvasMin, canvasMax);
        object.rotation.x = getRandomArbitrary(0, Math.PI);
        object.rotation.y = getRandomArbitrary(0, Math.PI);


        scene.add(object);

        controller.shapes.push({
          object, update: update().bind(null, object)
        });
      };

      return function (object) {
        initiliaseChessPiece(object);

        let object2 = object.clone();
        object2.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.material = new THREE.MeshPhongMaterial();
          }
        });
        initiliaseChessPiece(object2);
      }
    }

    let OBJloader = new THREE.OBJLoader(manager);
    OBJloader.load(modelpath + "/" + model.name + ".obj", handleModelLoaded(model));
  }

  function readModelInfoFromJSON() {
    let xmlhttp = new XMLHttpRequest();
    let url = "shapes.json";

    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let myArr = JSON.parse(this.responseText);
        loadModels(myArr.chess);
      }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

    function loadModels(models) {
      // iterate over JSON
      for (var i = 0; i < models.length; i++) {
        loadOBJ(models[i]);
      }
    }
  }

  readModelInfoFromJSON();

  return controller;
}