﻿function addModels(scene, topright) {
  let controller = makeController();

  let canvasMin = -topright.y;
  let canvasMax = topright.y;

  function update() {
    let speed = setSpeed();
    let rSpeed = setRotationSpeed();

    return function (mesh, topright) {
      let canvasMin = -topright.y;
      let canvasMax = topright.y;

      mesh.rotation.x += rSpeed.x;
      mesh.rotation.y += rSpeed.y;

      mesh.position.y += speed;

      if (mesh.position.x < -topright.x) {
        setPositionX(mesh, topright);
      }
      if (mesh.position.x > -(topright.x / 2)) {
        setPositionX(mesh, topright);
      }

      if (mesh.position.y > canvasMax * 1.1) {
        mesh.position.y = canvasMin * 1.1;
        speed = setSpeed();
        rspeed = setRotationSpeed();
        setPositionX(mesh, topright);

        switch (mesh.name) {
          case "companion_cube": break;
          case "companion-tetrahedron": break;
          case "duck": break;
          case "hearts":
          case "clubs":
          case "utah-teapot":
          case "diamonds":
          case "star":
          case "bunny":
          case "suzanne":
            let color = getRandomColor();
            mesh.traverse(function (child) {
              if (child instanceof THREE.Mesh) {
                child.material.color = color;
                child.castShadow = true;
                child.receiveShadow = true;
              }
            });
            break;
          default:
            mesh.material.color = getRandomColor();
        }
      }
    }
  }

  let href = window.location.href;
  let modelpath = "/models/";
  if (!href.includes("localhost"))
    modelpath = "/threejs-experiments" + modelpath;

  let manager = new THREE.LoadingManager();
  manager.onProgress = function (item, loaded, total) {
    //if (item.startsWith("data:"))
    //  console.log("data:", loaded, total);
    //else
    //  console.log(item, loaded, total);
  };


  function loadOBJ(model) {
    function handleModelLoaded(model) {
      return function (object) {

        if (model.image) {
          let texture = new THREE.Texture();
          let imageLoader = new THREE.ImageLoader(manager);
          imageLoader.load(modelpath + model.name + "/" + model.image, function (image) {
            texture.image = image;
            texture.needsUpdate = true;
          });
          object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
              child.material.map = texture;
            }
          });
        } else {

          let color = getRandomColor();
          object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
              child.material.color = color;
            }
          });
        }

        object.traverse(function (child) {
          if (child instanceof THREE.Mesh) {
            child.castShadow = true;
            child.receiveShadow = true;

          }
        });

        /*
        // an attempt to smooth out the models
        // doesn't work - further investigation needed
        if (model.name === "suzanne" || model.name === "utah-teapot") {
          object.traverse(function (child) {
            if (child instanceof THREE.Mesh) {
              if (child.material.shading === undefined) {
                let pts = child.geometry.getAttribute('position');
                let geom = new THREE.BufferGeometry();
                //geom.setFromPoints(pts.array);
                //let t = new THREE.Float32BufferAttribute(pts.array, 3);
                geom.setAttribute('position', new THREE.Float32BufferAttribute(pts.array, 3));
                geom = THREE.BufferGeometryUtils.mergeVertices(geom);
                geom.computeVertexNormals();
                child = new THREE.Mesh(geom);
                child.material = new THREE.MeshPhongMaterial({ color: getRandomColor() });

                //child.geometry = THREE.BufferGeometryUtils.mergeVertices(child.geometry);
                //child.geometry.computeVertexNormals();
                //child.material.flatShading = true;
                child.material.needsUpdate = true
              }
            }
          });

        }
        */

        let holder = new THREE.Object3D();

        holder.name = model.name;
        if (Array.isArray(model.scale)) {
          object.scale.set(...model.scale);
        } else {
          object.scale.set(model.scale, model.scale, model.scale);
        }
        object.position.set(...model.offset);
        setPositionX(holder, topright);
        holder.position.y = getRandomArbitrary(canvasMin, canvasMax);

        holder.add(object);
        scene.add(holder);

        controller.shapes.push({
          holder, update: update().bind(null, holder)
        });
      }
    }

    let OBJloader = new THREE.OBJLoader(manager);
    OBJloader.load(modelpath + model.name + "/" + model.name + "." + model.type, handleModelLoaded(model));
  }

  function loadGLTF(model) {
    function handleModelLoaded(model) {
      return function (gltf) {
        scene.add(gltf.scene);
        let object = gltf.scene.children[0];


        let holder = new THREE.Object3D();
        holder.position.set(0, 0, -20);
        holder.position.y = getRandomArbitrary(canvasMin, canvasMax);

        holder.name = model.name;

        object.scale.set(model.scale, model.scale, model.scale);
        object.position.set(...model.offset);
        setPositionX(holder, topright);

        holder.add(object);
        scene.add(holder);

        controller.shapes.push({
          holder, update: update().bind(null, holder)
        });
      }
    }

    let loader = new THREE.GLTFLoader(manager);
    loader.load(modelpath + model.name + "/" + model.name + "." + model.type, handleModelLoaded(model));
  }

  function readModelInfoFromJSON() {
    let xmlhttp = new XMLHttpRequest();
    let url = "shapes.json";

    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let myArr = JSON.parse(this.responseText);
        loadModels(myArr.models);
      }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

    function loadModels(models) {
      // iterate over JSON
      for (var i = 0; i < models.length; i++) {
        switch (models[i].type) {
          case "obj":
            loadOBJ(models[i]);
            break;
          case "gltf":
            loadGLTF(models[i]);
            break;
        }
      }
    }
  }

  readModelInfoFromJSON();

  return controller;
}