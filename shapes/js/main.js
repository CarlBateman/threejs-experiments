﻿"use strict";

window.addEventListener ?
  window.addEventListener("load", main, false) :
  window.attachEvent && window.attachEvent("onload", main);


function main() {
  function setCameraAndRenderer() {
    canvas.style.width = '';
    canvas.style.height = '';
    let w = canvas.clientWidth;
    let h = canvas.clientHeight;

    renderer.setSize(w, h);
    renderer.setViewport(0, 0, w, h);
    camera.aspect = w / h;
    camera.updateProjectionMatrix();


    let frustum = new THREE.Frustum();
    let projScreenMatrix = new THREE.Matrix4();

    camera.updateMatrixWorld();
    projScreenMatrix.multiplyMatrices(camera.projectionMatrix, camera.matrixWorldInverse);
    frustum.setFromProjectionMatrix(projScreenMatrix);


    // we don't know the order of the frutum planes so we'll have to test them all
    topright = new THREE.Vector3();
    let line = new THREE.Line3(new THREE.Vector3(-1000, 0, 0), new THREE.Vector3(1000, 0, 0));
    let target = new THREE.Vector3();
    for (let i = 0; i < 6; i++) {
      let res = frustum.planes[i].intersectLine(line, target);
      if (res !== null) {
        if (res.x > topright.x) topright.x = res.x;
      }
    }

    line = new THREE.Line3(new THREE.Vector3(0, -1000, 0), new THREE.Vector3(0, 1000, 0));
    target = new THREE.Vector3();
    for (let i = 0; i < 6; i++) {
      let res = frustum.planes[i].intersectLine(line, target);
      if (res !== null) {
        if (res.y > topright.y) topright.y = res.y;
      }
    }
  }

	let topright = new THREE.Vector3();
  window.addEventListener('resize', setCameraAndRenderer);

  let renderer = new THREE.WebGLRenderer({ antialias: true, alpha: true });
  //renderer.setClearColor(0xffffff, 0);
  renderer.domElement.id = "webgl-canvas";
  document.body.appendChild(renderer.domElement);

  let canvas = renderer.domElement;
  canvas.classList.add("centered");

  let scene = new THREE.Scene();

  let camera = new THREE.PerspectiveCamera(10, canvas.clientWidth / canvas.clientHeight, 10, 10000);
  camera.position.z = 1000;

  setCameraAndRenderer();

  let spotLight = new THREE.DirectionalLight(0xffffff, 0.5);
  spotLight.position.x = 20;
  spotLight.position.y = 20;
  scene.add(spotLight);

  //let light = new THREE.AmbientLight(0x404040);
  //scene.add(light);

  spotLight = new THREE.DirectionalLight(0xffffff, 0.5);
  spotLight.position.z = -20;
  //spotLight.position.y = 20;
  scene.add(spotLight);

  let light = new THREE.AmbientLight(0x404040);
  scene.add(light);


  let particles = addParticles(scene, topright);

  //let wave = addWave(scene);
  let controller;

  // Calculate milliseconds in a year
  const minute = 1000 * 60;
  const hour = minute * 60;
  const day = hour * 24;

  // Divide Time with a year
  const d = new Date();
  let days = Math.round(d.getTime() / day);
  days %= 3;
  
  switch (days) {
    case 1:
      controller = addModels(scene, topright);
      break;
    case 2:
      controller = addChess(scene, topright);
      break;
    case 0:
      controller = addGeometry(scene, topright);
      break;
  }

  let animate = function () {
    requestAnimationFrame(animate);

    controller.update(topright);

    particles.update(topright);
    //wave.update();

    renderer.render(scene, camera);
  };

  animate();
}