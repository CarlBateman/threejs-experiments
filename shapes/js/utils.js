﻿function getRandom(min, max) {
  return min + Math.random() * (max - min);
}

function getRandomRange(min, range) {
  return min + Math.random() * range;
}

function getRandomColor() {
  var r = Math.random();
  var g = Math.random();
  var b = Math.random();
  return new THREE.Color(r, g, b);
}

function getRandomArbitrary(min, max) {
  return Math.random() * (max - min) + min;
}

function makeController() {
  let controller = {
    shapes: [],
    update: function (topright) {
      for (let i = 0; i < this.shapes.length; i++) {
        this.shapes[i].update(topright);
      }
    },
  };

  return controller;
}

function setSpeed() {
  const minSpeed = 0.1;
  return minSpeed + (Math.random() * 0.9) / 2;
}

function setPositionX(mesh, topright) {
  const leftMost = -topright.x;
  const scale = topright.x / 2;
  mesh.position.x = leftMost + scale * Math.random();
}

function setRotationSpeed() {
  return {
    x: 0.01 * (0.5 - Math.random()),
    y: 0.01 * (0.5 - Math.random())
  }
}

