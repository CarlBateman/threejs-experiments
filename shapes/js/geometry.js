﻿function addGeometry(scene, topright) {
  let controller = makeController();

  let canvas = document.getElementById("webgl-canvas");
  let canvasMin = -canvas.clientHeight / 10;
  let canvasMax = canvas.clientHeight / 10;

  function update() {
    let speed = setSpeed();
    let rSpeed = setRotationSpeed();

    return function (mesh, topright) {
      let canvasMin = -topright.y;
      let canvasMax = topright.y;

      mesh.rotation.x += rSpeed.x;
      mesh.rotation.y += rSpeed.y;

      mesh.position.y += speed;

      if (mesh.position.x < -topright.x) {
        setPositionX(mesh, topright);
      }
      if (mesh.position.x > -(topright.x / 2)) {
        setPositionX(mesh, topright);
      }

      if (mesh.position.y > canvasMax * 1.1) {
        mesh.position.y = canvasMin * 1.1;
        speed = setSpeed();
        rspeed = setRotationSpeed();
        setPositionX(mesh);

        mesh.material.color = getRandomColor();
      }
    }
  }

  function readGeometyInfoFromJSON() {
    let xmlhttp = new XMLHttpRequest();
    let url = "shapes.json";

    xmlhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        let myArr = JSON.parse(this.responseText);
        addGeometries(myArr.geometries);
      }
    };
    xmlhttp.open("GET", url, true);
    xmlhttp.send();

    function addGeometries(geometries) {
      // iterate over JSON
      for (var i = 0; i < geometries.length; i++) {
        let material = new THREE.MeshPhongMaterial({ color: getRandomColor() });
        let g = geometries[i].geometry;
        let d = geometries[i].defaults;

        let geometry = new THREE[g + "Geometry"](...d);
        let mesh = new THREE.Mesh(geometry, material);
        mesh.position.y = getRandomArbitrary(canvasMin, canvasMax);

        setPositionX(mesh, topright);

        scene.add(mesh);

        controller.shapes.push({
          mesh, update: update().bind(null, mesh)
        });
      }
    }
  }

  readGeometyInfoFromJSON();

  return controller;
}