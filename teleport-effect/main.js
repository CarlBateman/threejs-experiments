﻿window.addEventListener('DOMContentLoaded', function () {
	let camera, scene, renderer, controls;

	let clip = { value: 5 };

	init();
	animate();

	function init() {
		scene = new THREE.Scene();

		const light1 = new THREE.AmbientLight(0xffffff);
		scene.add(light1);

		let light = new THREE.DirectionalLight(0xffffff, 0.5);
		light.position.x = -100;
		light.position.y = 200;
		light.position.z = 100;
		light.castShadow = true;
		light.shadow.camera = new THREE.OrthographicCamera(-10, 10, 20, -10, 0.5, 1000);
		scene.add(light);

		camera = new THREE.PerspectiveCamera(27, window.innerWidth / window.innerHeight, 1, 10000);
		camera.position.z = 35;
		camera.position.y = 25;
		camera.position.x = 11;


		let geometry = new THREE.PlaneGeometry(150, 150);

		let material = new THREE.MeshPhongMaterial({
			color: 0x448844,
			side: THREE.DoubleSide,
		});

		let ground = new THREE.Mesh(geometry, material);
		ground.receiveShadow = true;
		ground.rotation.x = -Math.PI / 2;
		scene.add(ground);

		let href = window.location.href;
		let modelpath = "/models/scifi-girl-01/scene.gltf";
		if (!href.includes("localhost"))
			modelpath = "/threejs-experiments" + modelpath;

		let loader = new THREE.GLTFLoader();
		loader.load(modelpath, function (gltf) {
			document.getElementById("loading-text").innerHTML = "Processing model";

			gltf.scene.traverse((g) => {
				if (g.castShadow !== undefined) {
					if (g.material !== undefined && g.material.name === "material") {
						g.receiveShadow = true;
					} else {
						g.castShadow = true;
						g.receiveShadow = true;
					}
				}
				if (g.material !== undefined) {
					if (g.material.name === "material")
						g.visible = false;

					g.material.transparent = true;

					if (g.material.name === "cloth")
						g.material.side = THREE.DoubleSide;

					g.material.onBeforeCompile = function (shader) {
						demo(shader, clip);
					};

					g.customDepthMaterial = new THREE.MeshDepthMaterial({
						depthPacking: THREE.RGBADepthPacking,
					});
					g.customDepthMaterial.onBeforeCompile = function (shader) {
						shader.uniforms.clip = clip;

						shader.vertexShader = 'uniform float clip;\n' + shader.vertexShader;
						shader.vertexShader = 'varying vec3 vPosition;\n' + shader.vertexShader;
						shader.vertexShader = shader.vertexShader.replace('void main() {', 'void main() {\nvPosition = position;\n');

						shader.fragmentShader = 'uniform float clip;\n' + shader.fragmentShader;
						shader.fragmentShader = 'uniform vec2 myValue;\n' + shader.fragmentShader;
						shader.fragmentShader = 'varying vec3 vPosition;\n' + shader.fragmentShader;
						shader.fragmentShader = shader.fragmentShader.replace('void main() {', 'void main() {\nif(vPosition.z < clip) discard;\n');
					};
				}
			});

			document.getElementById("loading").remove();


			let bar = gltf.scene.children[0].children[0].children[0];
			bar.rotation.x = -Math.PI / 2;
			bar.rotation.z = Math.PI;
			scene.add(bar);
		});

		renderer = new THREE.WebGLRenderer({ antialias: true });
		renderer.setPixelRatio(window.devicePixelRatio);
		renderer.setSize(window.innerWidth, window.innerHeight);
		renderer.shadowMap.enabled = true;
		renderer.setClearColor(0xafeeee, 1);

		document.body.prepend(renderer.domElement);

		controls = new THREE.OrbitControls(camera, renderer.domElement);
		controls.target.x = -2;
		controls.target.y = 8;
		controls.target.z = -5;

		window.addEventListener('resize', onWindowResize, false);
	}

	function onWindowResize() {
		let width = window.innerWidth;
		let height = window.innerHeight;

		camera.aspect = width / height;
		camera.updateProjectionMatrix();

		renderer.setSize(width, height);
	}

	function animate() {
		requestAnimationFrame(animate);

		clip.value += 0.1;
		if (clip.value > 25) {
			clip.value = -5;
		}

		render();
		//stats.update();
		controls.update();
	}

	function render() {
		renderer.render(scene, camera);
	}
});