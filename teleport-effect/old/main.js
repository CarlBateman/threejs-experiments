﻿/// <reference path="../scripts/three.js" />
window.addEventListener('DOMContentLoaded', function () {
  let plane;
  let scene;
  let light;


  main();

  function setupBox() {
    let geometry = new THREE.PlaneGeometry(15, 20, 100, 100);

    let material = createMaterial("vertex-shader", "fragment-shader");
    //let material = new THREE.MeshBasicMaterial();

    //material.wireframe = true;
    plane = new THREE.Mesh(geometry, material);
    scene.add(plane);
  }

  function main() {
    scene = new THREE.Scene();
    //scene.background = 
    let camera = new THREE.PerspectiveCamera(60, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.x = 7.25;
    camera.position.y = 20;
    camera.position.z = 13.5;

    camera.rotation.x = -.6;
    camera.rotation.y = .37;
    camera.rotation.z = .24;



    let renderer = new THREE.WebGLRenderer();
    renderer.setClearColor(0x1B1D1FC, 1);
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);

    const light1 = new THREE.AmbientLight(0xffffff);
    scene.add(light1);

    light = new THREE.DirectionalLight(0xffffff, 0.5);
    //light.position.x = -100;
    light.position.y = 100;
    light.position.z = 100;
    scene.add(light);

    let geometry = new THREE.PlaneGeometry(50, 50);
    let material = new THREE.MeshStandardMaterial({ color: 0x2194CE, side: THREE.DoubleSide });
    let bar = new THREE.Mesh(geometry, material);
    bar.rotation.x = -Math.PI / 2;
    scene.add(bar);



    const gltfLoader = new THREE.GLTFLoader();
    gltfLoader.load("/models/scifi-girl-01/scene.gltf", (gltf) => {
      gltf.scene.traverse((g) => {
        //if (g.isMesh) {
        //  g.position.y = -15;
        //  g.rotation.x = -Math.PI / 2;
        //  g.rotation.z = Math.PI;

        //  scene.add(g);
        //}
        gltf.scene.traverse((g) => {
          if (g.isMesh && g.material.name === "cloth") {
            let cloth = g;
            //cloth.material.specular.multiplyScalar(100);
            cloth.material.emissiveMap = cloth.material.specularMap;
            cloth.material.emissiveIntensity = 50;
            //scene.add(g);
            //g.material = material;
          }
        });

        let bar = gltf.scene.children[0].children[0].children[0];
        bar.rotation.x = -Math.PI / 2;
        bar.rotation.z = Math.PI;
        scene.add(bar);
      });


      //material = new THREE.MeshStandardMaterial({ color: 'hsl(180,50%,50%)' });
      //material.roughness = 0.5;
      //material.metalness = 0.5;
      // children[""0""].children[""0""]

      //let cnt = 0;
      //gltf.scene.traverse((g) => {
      //  if (g.isMesh)
      //    console.log("Mesh ", g.name);
      //  else
      //    console.log("Not mesh", g.name);
      //});

      //let bar = gltf.scene.children[0].children[0].children[0];
      ////bar.position.y = -15;
      //bar.rotation.x = -Math.PI/2;
      //bar.rotation.z = Math.PI;
      ////bar.visible = false;
      ////bar.material = material;
      //scene.add(bar);

      ////setup();
    });

    let controls = new THREE.OrbitControls(camera, renderer.domElement);
    //controls.screenSpacePanning = true;

    (function animate() {
      requestAnimationFrame(animate);
      renderer.clear();
      renderer.render(scene, camera);
      controls.update();
    })();
  }

  function createMaterial(vertexShader, fragmentShader) {
    let vertShader = document.getElementById(vertexShader).innerHTML;
    let fragShader = document.getElementById(fragmentShader).innerHTML;

    let attributes = {};
    let uniforms = {};

    let meshMaterial = new THREE.ShaderMaterial({
      //uniforms: uniforms,
      //attributes: attributes,
      vertexShader: vertShader,
      fragmentShader: fragShader,
    });
    meshMaterial.side = THREE.DoubleSide;
    return meshMaterial;
  }
});