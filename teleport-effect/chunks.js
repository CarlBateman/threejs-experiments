﻿let CHUNK = function (shader) {
  this.addChunkFrag = function (insertionPoint, chunk, before) {
    if (before) {
      let code = chunk + '\n' + insertionPoint;
      shader.fragmentShader = shader.fragmentShader.replace(insertionPoint, code);
    } else {
      let code = insertionPoint + '\n' + chunk;
      shader.fragmentShader = shader.fragmentShader.replace(insertionPoint, code);
    }
  };
  this.addChunkVert = function (insertionPoint, chunk) {
    let code = insertionPoint + '\n' + chunk;
    shader.vertexShader = shader.vertexShader.replace(insertionPoint, code);
  };
  this.addVarying = function (varying) {
    shader.fragmentShader = "varying " + varying + '\n' + shader.fragmentShader;
    shader.vertexShader = "varying " + varying + '\n' + shader.vertexShader;
  };
  this.addUniform = function (uniform) {
    shader.fragmentShader = "uniform " + uniform + '\n' + shader.fragmentShader;
    shader.vertexShader = "uniform " + uniform + '\n' + shader.vertexShader;
  };
  this.addUniformFrag = function (uniform) {
    shader.fragmentShader = "uniform " + uniform + '\n' + shader.fragmentShader;
  };
  this.addUniformVert = function (uniform) {
    shader.vertexShader = "uniform " + uniform + '\n' + shader.vertexShader;
  };
};

function demo(shader, clip) {
  shader.uniforms.clip = clip;
  let teleportChunk = new CHUNK(shader);

  const main = "void main() {";
  //const diffuse = "vec4 diffuseColor = vec4( diffuse, opacity );";
  const diffuse = "#include <map_fragment>";

  teleportChunk.addChunkVert(main, 'vPosition = position;');
  teleportChunk.addChunkFrag(main, "//if(vPosition.z <= clip) discard;");

  chunk = `
//if(vPosition.z > clip + 0.0) 
{
  //float step = abs(vPosition.z - clip)/2.;
  diffuseColor = getColor(vPosition, diffuseColor, clip);
//if(diffuseColor.a <= 0.)
//discard;

}`;
  teleportChunk.addChunkFrag(diffuse, chunk);

  teleportChunk.addChunkFrag(main, getColor(), true);


  teleportChunk.addVarying('vec3 vPosition;');

  teleportChunk.addUniformFrag("float clip;");
}

function getColor() {
  return `
    //#define PI 3.14159
    #define TWO_PI (PI*2.0)
    #define N 68.5

    vec4 getColor(vec3 pos, vec4 diffuseColor, float clip) {
      vec3 v = pos.xyz * 1.;
      v.x-=10.0;
      v.y-=200.0;
      //v.z-=20.0;
      float col = 0.0;

      for(float i = 0.0; i < N; i++) {
        float a = i * (TWO_PI/N) * 61.95;
        col += cos(TWO_PI*(v.y * sin(a*a) + v.x * sin(a) + v.z * cos(2.*a) ));
        
      }

      col /= 5.0;

      float gradient = pos.z;
      gradient /= 20.;
      //gradient += 10.;

      clip /= 20.0;

      float screen = smoothstep(clip-0.1, clip, gradient);
      float screen1 = smoothstep(clip, clip+0.1, gradient);

      col *= screen - screen1;
      col += screen1 * 2.;

      if(col < 0.01) col = 0.;
      if(col > 0.99) return diffuseColor;

      float edge = col * 100.;
      edge = edge + (ceil(edge) > 0.5 ? edge : 1.0) ;
      edge = ceil(edge) * 0.01;
      edge =edge -  col;

      //vec4 newColor = vec4(edge*diffuseColor.r * 2., edge*diffuseColor.g*2., col*diffuseColor.g, diffuseColor.a * col);


      vec4 newColor = vec4(1., 1., 0., col);
      //newColor = vec4(col, col, 0., col);
  
      return newColor;
    }
`;
}
